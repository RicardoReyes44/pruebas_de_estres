'''
Created on 28 nov. 2020

@author: RSSpe
'''

import time


class MetodosOrdenamiento:
    
    def __init__(self):
        self.recorridos = 0
        self.comparaciones = 0
        self.intercambios = 0
    
    def burbuja1(self, arreglo):
        inicio = time.time()
        for i in range(1, len(arreglo)):
            self.recorridos+=1
            self.comparaciones+=1
            for j in range(0, len(arreglo)-i):
                self.comparaciones+=1
                self.recorridos+=1
                if arreglo[j]>arreglo[j+1]:
                    self.comparaciones+=1
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    arreglo[j+1] = aux
                    self.intercambios+=1
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

    def burbuja2(self, arreglo):
        i = 0
        inicio = time.time()
        while i<len(arreglo):
            self.recorridos+=1
            self.comparaciones+=1
            j=i
            while j<len(arreglo):
                self.comparaciones+=1
                self.recorridos+=1
                if arreglo[i]>arreglo[j]:
                    aux = arreglo[i]
                    self.comparaciones+=1
                    arreglo[i] = arreglo[j]
                    arreglo[j] = aux
                    self.intercambios+=1
                j+=1
            i+=1
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0
        
    def burbuja3(self, arreglo):
        cont = 1
        inicio = time.time()
        while cont<len(arreglo):
            self.recorridos+=1
            self.comparaciones+=1
            for j in range(0, len(arreglo)-cont):
                self.recorridos+=1
                self.comparaciones+=1
                if arreglo[j]>arreglo[j+1]:
                    self.comparaciones+=1
                    self.intercambios+=1
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    arreglo[j+1] = aux
            cont+=1
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

    def insercion(self, numeros):
        aux=0
        inicio = time.time()
        for i in range(1, len(numeros)):
            self.recorridos+=1
            self.comparaciones+=1
            aux = numeros[i]
            j=(i-1)
            while (j>=0 and numeros[j]>aux):
                self.comparaciones+=1
                self.recorridos+=1
                numeros[j+1] = numeros[j]
                numeros[j] = aux
                self.intercambios+=1
                j-=1
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

    def shellSort(self, numeros):
        intervalo = int(len(numeros)/2)
        inicio = time.time()
        while(intervalo>0):
            self.recorridos+=1
            self.comparaciones+=1
            for i in range(int(intervalo), len(numeros)):
                self.comparaciones+=1
                self.recorridos+=1
                j=i-int(intervalo)
                while(j>=0):
                    self.comparaciones+=1
                    self.recorridos+=1
                    k=j+int(intervalo)
                    if numeros[j]<=numeros[k]:
                        j-=1
                    else:
                        self.intercambios+=1
                        aux=numeros[j]
                        numeros[j]=numeros[k]
                        numeros[k]=aux
                        j-=int(intervalo)
                    self.comparaciones+=1
            intervalo=int(intervalo)/2
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

    def ordenamientoSeleccion(self, numeros):
        inicio = time.time()
        for i in range(len(numeros)):
            self.comparaciones+=1
            self.recorridos+=1
            for j in range(i, len(numeros)):
                self.comparaciones+=1
                self.recorridos+=1
                if(numeros[i] > numeros[j]):
                    self.comparaciones+=1
                    orden = numeros[i]
                    numeros[i] = numeros[j]
                    numeros[j] = orden
                    self.intercambios+=1
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

    def ejecutar(self, arreglo2):
        inicio = time.time()
        self.quickSort(arreglo2, 0, len(arreglo2)-1)
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0
    
    def quickSort(self, numeros, izq, der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0

        while i<j:
            self.recorridos+=1
            self.comparaciones+=1
            while numeros[i]<=pivote and i<j:
                self.recorridos+=1
                i+=1
                self.comparaciones+=1
            while numeros[j]>pivote:
                self.recorridos+=1
                j-=1
                self.comparaciones+=1
            if i<j:
                self.comparaciones+=1
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux
                self.intercambios+=1

        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if izq<j-1:
            self.comparaciones+=1
            self.quickSort(numeros, izq, j-1)
        if j+1<der:
            self.comparaciones+=1
            self.quickSort(numeros, j+1, der)

    def countingSort(self, arr, exp1):
        n = len(arr)
        output = [0] * (n)
        count = [0] * (10)

        for i in range(0, n):
            self.recorridos+=1
            self.comparaciones+=1
            index = (arr[i]/exp1)
            count[int((index)%10)] += 1
        for i in range(1,10):
            self.comparaciones+=1
            count[i] += count[i-1]
            self.recorridos=+1
        i = n-1
        while i>=0:
            self.comparaciones+=1
            self.recorridos=+1
            index = (arr[i]/exp1)
            output[ count[ int((index)%10) ] - 1] = arr[i]
            count[int((index)%10)] -= 1
            i -= 1
            self.intercambios+=1

        i = 0
        for i in range(0,len(arr)):
            self.comparaciones+=1
            arr[i] = output[i]
            self.recorridos=+1

    # Method to do Radix Sort
    def radixSort(self, arr):
        max1 = max(arr)
        exp = 1
        inicio = time.time()
        while max1/exp > 0:
            self.comparaciones+=1
            self.recorridos+1
            self.countingSort(arr,exp)
            exp *= 10
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

    def intercalacion(self, a1, a2):
        a3 : list = []
        cont : int = 0
        cont2 : int = 0

        inicio = time.time()
        while cont<len(a1) and cont2<len(a2):
            self.comparaciones+=1
            self.recorridos+=1
            if a1[cont]<=a2[cont2]:
                a3.append(a1[cont])
                cont+=1
            else:
                a3.append(a2[cont2])
                cont2+=1
            self.intercambios+=1
            self.comparaciones+=1

        while len(a1)!=cont:
            self.recorridos+=1
            self.intercambios+=1
            a3.append(a1[cont])
            self.comparaciones+=1
            cont+=1

        while len(a2)!=cont2:
            self.recorridos+=1
            self.intercambios+=1
            a3.append(a2[cont2])
            self.comparaciones+=1
            cont2+=1
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0

        return a3
    
    def ejecutarMD(self, array):
        
        inicio = time.time()
        
        array = self.ordenamientoMezclaDirecta(array)
        
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0
        
        return array
    
    def ordenamientoMezclaDirecta(self,array):
        
        mitad=len(array)//2
        
        if len(array)>=2:
            self.comparaciones+=1
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.ordenamientoMezclaDirecta(arregloIz)
            self.ordenamientoMezclaDirecta(arregloDer)
            
            #Hasta aqui es la divicion de todos los elemntos hasta que llege a ser igual a 1
            while(len(arregloDer)>0 and len(arregloIz)>0):
                self.comparaciones+=1
                self.recorridos+=1
                if(arregloIz[0]< arregloDer[0]):# si la pocicion de la izquierda es menor a la derecha
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
                self.comparaciones+=1
                self.intercambios+=1
            #Hace que siempre se este actualizando ya que se elimina la pocicion
            
            #Ahora esto es por si llegan a quedar elementos sobrantes
            while len(arregloIz)>0:
                self.recorridos+=1
                self.comparaciones+=1
                array.append(arregloIz.pop(0))
                self.intercambios+=1
            
            while len(arregloDer)>0:
                self.comparaciones+=1
                self.intercambios+=1
                self.recorridos+=1
                array.append(arregloDer.pop(0))
        
        return array
    
    def mezclaDirecta2(self, array):
        mitad=len(array)//2
        
        if len(array)>=2:
            self.comparaciones+=1
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta2(arregloIz)
            self.mezclaDirecta2(arregloDer)
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                self.comparaciones+=1
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
                self.recorridos+=1
                self.intercambios+=1
           
            while len(arregloIz)>0:
                self.comparaciones+=1
                self.recorridos+=1
                self.intercambios+=1
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                self.comparaciones+=1
                self.recorridos+=1
                self.intercambios+=1
                array.append(arregloDer.pop(0))

    def mezclaNatural(self, numeros):
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        inicio = time.time()
        while(not ordenado):
            self.comparaciones+=1
            ordenado = True
            self.recorridos+=1
            izquierdo =0
            while(izquierdo<derecho):
                self.comparaciones+=1
                self.recorridos+=1
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                    self.recorridos+=1
                    self.comparaciones+=1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                    self.recorridos+=1
                    self.comparaciones+=1
                if(der<=derecho):
                    self.comparaciones+=1
                    self.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
        fin = time.time()
        print(f"Recorridos: {self.recorridos}")
        print(f"Intercambios: {self.intercambios}")
        print(f"Comparaciones: {self.comparaciones}")
        print(f"Tiempo: {fin-inicio}")
        self.comparaciones = self.intercambios = self.recorridos = 0
        return numeros
