'''
Created on 28 nov. 2020

@author: RSSpe
'''

from metodos_ordenamiento import MetodosOrdenamiento
import random

mil = [random.randint(0, 101) for i in range(0, 1000)]
diezMil = [random.randint(0, 101) for i in range(0, 10000)]
cienMil = [random.randint(0, 101) for i in range(0, 100000)]
    
def quickSort(numeros, izq, der):
    pivote = numeros[izq]
    i = izq
    j = der
    aux = 0

    while i<j:
        while numeros[i]<=pivote and i<j:
            i+=1
        while numeros[j]>pivote:
            j-=1
        if i<j:
            aux = numeros[i]
            numeros[i] = numeros[j]
            numeros[j] = aux

mo = MetodosOrdenamiento()


while True:

    print("------------MENU--------------")
    print("1.- Probar con 1000 elementos")
    print("2.- Probar con 10000 elementos")
    print("3.- Probar con 100000 elementos")
    print("4.- Salir")
    
    try:
        opcion = int(input("Ingresa opcion: "))
    
        if opcion==1:
            print("-------------Metodo Burbuja 1--------------")
            mo.burbuja1(mil.copy())
            print()
            print("-------------Metodo Burbuja 2--------------")
            mo.burbuja2(mil.copy())
            print()
            print("-------------Metodo Burbuja 3--------------")
            mo.burbuja3(mil.copy())
            print()
            print("-------------Insercion--------------")
            mo.insercion(mil.copy())
            print()
            print("-------------Seleccion--------------")
            mo.ordenamientoSeleccion(mil.copy())
            print()
            print("-------------Quick--------------")
            mo.ejecutar(mil.copy())
            print()
            print("-------------Shell--------------")
            mo.shellSort(mil.copy())
            print()
            print("-------------Radix--------------")
            mo.radixSort(mil.copy())
            print()
            print("-------------Intercalacion--------------")
            nuevo = mil.copy()
            nuevo2 = [random.randint(0, 101) for i in range(0, 10000)]
            quickSort(nuevo, 0, len(nuevo)-1)
            quickSort(nuevo2, 0, len(nuevo2)-1)
            mo.intercalacion(nuevo, nuevo2)
            print()
            print("-------------Mezcla directa--------------")
            mo.ejecutarMD(mil.copy())
            print()
            print("-------------Mezcla natural--------------")
            mo.mezclaNatural(mil.copy())
            print()

        elif opcion==2:
            print("-------------Metodo Burbuja 1--------------")
            mo.burbuja1(diezMil.copy())
            print()
            print("-------------Metodo Burbuja 2--------------")
            mo.burbuja2(diezMil.copy())
            print()
            print("-------------Metodo Burbuja 3--------------")
            mo.burbuja3(diezMil.copy())
            print()
            print("-------------Insercion--------------")
            mo.insercion(diezMil.copy())
            print()
            print("-------------Seleccion--------------")
            mo.ordenamientoSeleccion(diezMil.copy())
            print()
            print("-------------Quick--------------")
            mo.ejecutar(diezMil.copy())
            print()
            print("-------------Shell--------------")
            mo.shellSort(diezMil.copy())
            print()
            print("-------------Radix--------------")
            mo.radixSort(diezMil.copy())
            print()
            print("-------------Intercalacion--------------")
            nuevo = diezMil.copy()
            nuevo2 = [random.randint(0, 101) for i in range(0, 10000)]
            quickSort(nuevo, 0, len(nuevo)-1)
            quickSort(nuevo2, 0, len(nuevo2)-1)
            mo.intercalacion(nuevo, nuevo2)
            print()
            print("-------------Mezcla directa--------------")
            mo.ejecutarMD(diezMil.copy())
            print()
            print("-------------Mezcla natural--------------")
            mo.mezclaNatural(diezMil.copy())
            print()
    
        elif opcion==3:
            print("-------------Metodo Burbuja 1--------------")
            mo.burbuja1(cienMil.copy())
            print()
            print("-------------Metodo Burbuja 2--------------")
            mo.burbuja2(cienMil.copy())
            print()
            print("-------------Metodo Burbuja 3--------------")
            mo.burbuja3(cienMil.copy())
            print()
            print("-------------Insercion--------------")
            mo.insercion(cienMil.copy())
            print()
            print("-------------Seleccion--------------")
            mo.ordenamientoSeleccion(cienMil.copy())
            print()
            print("-------------Quick--------------")
            mo.ejecutar(cienMil.copy())
            print()
            print("-------------Shell--------------")
            mo.shellSort(cienMil.copy())
            print()
            print("-------------Radix--------------")
            mo.radixSort(cienMil.copy())
            print()
            print("-------------Intercalacion--------------")
            nuevo = cienMil.copy()
            nuevo2 = [random.randint(0, 101) for i in range(0, 10000)]
            quickSort(nuevo, 0, len(nuevo)-1)
            quickSort(nuevo2, 0, len(nuevo2)-1)
            mo.intercalacion(nuevo, nuevo2)
            print()
            print("-------------Mezcla directa--------------")
            mo.ejecutarMD(cienMil.copy())
            print()
            print("-------------Mezcla natural--------------")
            mo.mezclaNatural(cienMil.copy())
            print()

        elif opcion==4:
            print("---------FIN---------")
            break
    
        else:
            print("No existe esa opcion")
        print("------------------------------------------")
    
    except ValueError as error:
        print("Datos invalidos <",error,">, por favor prueba de nuevo\n")
